﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            //Fibonacci series is a series that adds the previous 2 numbers
            //to make the 3rd number
            int a = 0; //variable a will take the first number
            int b = 1;//variable b will take the 2nd number
            int c; //c will take the 3rd number (sum of a and b)

            //Prompt user to input number of fibonacci series desired
            Console.WriteLine("Enter required number");

            //Converts user input to integer
            int n = Convert.ToInt32(Console.ReadLine());
            //Outputs result instruction
            Console.Write("First " + n + " Fibonacci series : 1     ");

            for (int i = 1; i < n; i++)
            {
                c = a + b;
                a = b;
                b = c;
                Console.Write(c +"\t");
            } Console.ReadKey();

        }
    }
}
